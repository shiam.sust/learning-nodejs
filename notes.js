const fs = require('fs')
const chalk = require('chalk')

const addNote = (title, body) => {
    const notes = loadNotes()
    // const duplicateNotes = notes.filter((notes) => notes.title === title)
    const duplicateNote = notes.find((note) => note.title === title)

    // const duplicateNotes = notes.filter(function(notes){
    //     return notes.title === title
    // })

    if(!duplicateNote){
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log(chalk.green('New note added!'))
    } else {
        console.log(chalk.red('Note title already taken!'))
    }  
}

const removeNote = (title) => {
    const notes = loadNotes()
    const notesToKeep = notes.filter((note) => note.title !== title)

    // const notesToKeep = notes.filter(function(note){
    //     return note.title !== title
    // })

    if(notes.length === notesToKeep.length){
        console.log(chalk.red('No note found!'))
        saveNotes(notesToKeep)
    }else{
        console.log(chalk.green('Note removed!'))
    }
}

const listNotes = () => {
    const notes = loadNotes()
    console.log(chalk.green.inverse('Your Notes:'))
    notes.forEach((note) => {
        console.log(chalk.blue(note.title))
    })
}

const readNote = (title) => {
    const notes = loadNotes()
    const desireNote = notes.find((note) => note.title === title)
    if(desireNote){
        console.log(chalk.green.inverse(desireNote.title))
        console.log(desireNote.body)
    }else{
        console.log(chalk.red('No note found!'))
    }
}

const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJSON)
}

const loadNotes = () => {
    try{
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    }catch(e){
        return []
    }
}

module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}