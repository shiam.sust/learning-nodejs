const notes = require('./notes')
const chalk = require('chalk')
const yargs = require('yargs')

// console.log(getNote('my new note'))
// console.log(chalk.green('Success!'))
// var bol = chalk.bold('Success!')
// console.log(chalk.green(bol))

//console.log(chalk.bold.green('Success!'))
//console.log(process.argv)
yargs.command({
    command: 'add',
    describe: 'to add a new note',
    builder:{
        title:{
            describe: 'note title',
            demandOption: true,
            type: 'string'
        },
        body:{
            describe: 'note body',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv){
        notes.addNote(argv.title, argv.body)
    }
})

yargs.command({
    command: 'remove',
    describe: 'to remove a new command',
    builder: {
        title:{
            describe: 'note title',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv){
        notes.removeNote(argv.title)
    }
})

yargs.command({
    command: 'list',
    describe: 'to list notes',
    handler(argv){
        notes.listNotes()
    }
})

yargs.command({
    command: 'read',
    builder: {
        title:{
            describe: 'note title',
            demandOption: true,
            type: 'string'
        }
    },
    describe: 'to describe a note',
    handler(argv){
        notes.readNote(argv.title)
    }
})

yargs.parse()
// console.log(yargs.argv)


